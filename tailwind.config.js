module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      colors: {
        'omni-red': '#E63946',
        'omni-white': '#F1FAEE',
        'omni-light': '#A8DADC',
        'omni-medium': '#457B9D',
        'omni-dark': '#1D3557',
      },
      fontFamily: {
        'sans': [ 'Raleway' ],
      }
    }
  },
  plugins: [],
}