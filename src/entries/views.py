from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, render

from .models import Entry

def index(request):
    latest_entry_list = Entry.objects.order_by("-pub_date")[:5]
    context = {
        "latest_entry_list": latest_entry_list,
    }
    return render(request, "entries/index.html", context)

def detail(request, entry_id):
    entry = get_object_or_404(Entry, pk=entry_id)
    return render(request, "entries/detail.html", {"entry": entry})