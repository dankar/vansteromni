from django.core.management.base import BaseCommand, CommandError
from feeds.models import Feed
from feeds.functions import parse_feed

class Command(BaseCommand):
    help = "Scrape one or more feeds"

    def add_arguments(self, parser):
        parser.add_argument("feed_ids", nargs="+", type=str)

    def handle(self, *args, **options):
        feed_ids = []
        if options["feed_ids"][0] == "all":
            feeds = Feed.objects.all()
            for feed in feeds:
                feed_ids.append(int(feed.pk))
        else:
            for feed_id in options["feed_ids"]:
                try:
                    feed_ids.append(int(feed_id))
                except:
                    raise CommandError("feed_ids must be 'all' or integers")

        for feed_id in feed_ids:
            try:
                feed = Feed.objects.get(pk=feed_id)
                self.stdout.write(
                    self.style.SUCCESS('Scraping feed "%s"...' % feed.name)
                )
                parse_feed(feed)
                self.stdout.write(
                    self.style.SUCCESS('Successfully scraped feed "%s"' % feed.name)
                )
            except Feed.DoesNotExist:
                print("Feed {} does not exist".format(feed_id))
            except Exception:
                print("Failed to scrape feed {}".format(feed_id))

