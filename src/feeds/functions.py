import xml.etree.ElementTree as ET
import requests
import email.utils
import datetime
import time
from django.conf import settings
from django.utils import timezone

from .models import Item

"""
First we have to convert the weird rss format RFC 822 (Wed, 19 Jul 2023 07:49:27 +0000) to
YYYY-MM-DD HH:MM
"""
def feed_date_format_to_django_date(feed_date_str):
    try:
        if not feed_date_str:
            return
   
        date_parsed = email.utils.parsedate(feed_date_str)
        t = time.mktime(date_parsed)
        dt = datetime.datetime.fromtimestamp(t)
        aware_datetime = timezone.make_aware(dt)

        return aware_datetime
    except Exception as e:
        return None

# Separate function so we easily can mock it
def get_now():
    n = timezone.now()
    return n

def parse_feed(feed):
    print("Parsing feed {}".format(feed.name))

    print("Fetching data from {}".format(feed.url))
    data = get_feed_data(feed.url)
    if not data:
        print("Could not fetch data from {}".format(feed.url))
        return  

    items = feed_string_to_items(data, feed.item_feed_map)
    parsed = len(items)
    print("Got {} items from feed".format(parsed))
    new_items = 0
    errors = 0
    for item in items:
        try:
            existing_items = Item.objects.filter(item_id=item["item_id"])
            if existing_items.count() > 0:
                print("Item already exist with item_id {}".format(item["item_id"]))
                continue

            new_item = Item(
                feed=feed,
                title=item["title"],
                link=item["link"],
                item_id=item["item_id"],
                imported=get_now(),
                published=feed_date_format_to_django_date(item["published"]),
                updated=feed_date_format_to_django_date(item["updated"]),
                content=item["content"] if item["content"] else "",
                summary=item["summary"],
                image_url=item["image_url"] if item["image_url"] else ""
            )
            new_item.save()
            print("New item: {}".format(new_item))
            new_items += 1
        except Exception as e:
            print("Error occured with item:")
            print(item)
            print(e)
            errors += 1

    print("Parsed {} items. New items: {}. Errors: {}".format(parsed, new_items, errors))

def get_feed_data(feed_url):
    try:
        r = requests.get(feed_url)
        return r.text
    except Exception as e:
        print("Error reading url".format(feed_url))
        return None

def feed_string_to_items(feed_string, item_feed_map):
    root = ET.fromstring(feed_string)
    l = []
    items = root.findall(item_feed_map.items)

    for entry in items:
        try:
            title = entry.findtext(item_feed_map.title)
            link = entry.findtext(item_feed_map.link) 
            item_id = entry.findtext(item_feed_map.item_id)
            published = entry.findtext(item_feed_map.published)
            updated = entry.findtext(item_feed_map.updated)
            content = entry.findtext(item_feed_map.content)
            summary = entry.findtext(item_feed_map.summary)
            image_url = entry.findtext(item_feed_map.image_url)
            item = {
                "title": title,
                "link": link,
                "item_id": item_id,
                "published": published,
                "updated": updated,
                "content": content,
                "summary": summary,
                "image_url": image_url,
            }
            l.append(item)
        except Exception as e:
            print("Error parsing")
            print(e)

    return l