from django.test import TestCase

import httpretty
from .models import Feed, ItemFeedMap
from .functions import feed_string_to_items, parse_feed, get_feed_data, feed_date_format_to_django_date
from .tests_data import data_konstellationen_feed_sample, data_arbetaren_feed_sample, data_etc_feed_sample

class HelperFunctionsTests(TestCase):
    def test_feed_date_format_to_django_date(self):
        """
        Test formatting
        """
        new_format_string = feed_date_format_to_django_date("Wed, 19 Jul 2023 07:49:27 +0000")
        print("new_format_string")
        print(new_format_string)
        self.assertIs(1, 1)

class FunctionsTests(TestCase):
    @httpretty.activate(verbose=True, allow_net_connect=False)
    def test_get_feed_data(self):
        httpretty.register_uri(
            httpretty.GET,
            "https://www.arbetaren.se/feed/",
            body=data_arbetaren_feed_sample
        )
        data = get_feed_data("https://www.arbetaren.se/feed/")
        print(data)
        self.assertIs(1, 1)

    @httpretty.activate(verbose=True, allow_net_connect=False)
    def test_parse_feed(self):
        httpretty.register_uri(
            httpretty.GET,
            "https://www.arbetaren.se/feed/",
            body=data_arbetaren_feed_sample
        )
        item_feed_map = ItemFeedMap(
            item_feed_map_name="Arbetaren",
            items="channel/item",
            title="title",
            link="link",
            item_id="guid",
            published="pubDate",
            updated="",
            content="content",
            summary="description",
            image_url="media/media",
        )
        item_feed_map.save()
        feed = Feed(name="Arbetaren", url="https://www.arbetaren.se/feed/", description="Arbetaren feed", item_feed_map=item_feed_map)
        feed.save()
        parse_feed(feed)
        self.assertIs(1, 1)

    def test_feed_string_to_items_arbetaren_sample(self):
        """
        Test at sample from arbetaren
        """
        item_feed_map = ItemFeedMap(
            items="channel/item",
            title="title",
            link="link",
            item_id="guid",
            published="pubDate",
            updated="",
            content="content",
            summary="description",
            image_url="media/media",
        )
        item_feed_map.save()
        items = feed_string_to_items(data_arbetaren_feed_sample, item_feed_map)
        self.assertIs(len(items), 1)
        self.assertEqual("Därför måste vi prata om sexbarnsfamiljernas lyxande", items[0]["title"])

    def test_feed_string_to_items_etc_sample(self):
        """
        Test at sample from etc
        """
        item_feed_map = ItemFeedMap(
            items="channel/item",
            title="title",
            link="link",
            item_id="guid",
            published="pubDate",
            updated="",
            content="content",
            summary="description",
            image_url="enclosure",
        )
        item_feed_map.save()
        items = feed_string_to_items(data_etc_feed_sample, item_feed_map)
        self.assertIs(len(items), 2)
        self.assertEqual("Moderat toppnamn döms till fängelse efter flera våldsbrott", items[0]["title"])
