from django.urls import path

from . import views

app_name = "feeds"

urlpatterns = [
    path("", views.index, name="index"),
    path("about", views.about, name="about"),
    path("contact", views.contact, name="contact"),
    path("curated", views.curated, name="curated"),
    path("all", views.all, name="all"),
    path("<int:feed_item_id>/", views.detail, name="detail"),
    path("feed", views.list_feeds, name="list_feeds"),
    path("feed/<int:feed_id>/all", views.feed_all, name="feed_all"),
    path("feed/<int:feed_id>/curated", views.feed_curated, name="feed_curated"),
    path("api/like/<int:feed_item_id>/", views.api_like, name="api_like"),
]
