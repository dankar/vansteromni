data_konstellationen_feed_sample = """<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <title>Konstellationen</title>
  <icon>https://konstellationen.org/icon.png</icon>
  <subtitle>Kopplar samman vänstern</subtitle>
  <link href="https://konstellationen.org/atom.xml" rel="self"/>
  
  <link href="https://konstellationen.org/"/>
  <updated>2023-07-06T10:20:31.007Z</updated>
  <id>https://konstellationen.org/</id>
  
  <author>
    <name>Kamratdataföreningen Konstellationen</name>
    
  </author>
  
  <generator uri="https://hexo.io/">Hexo</generator>
  
  <entry>
    <title>Bortom Big Tech sociala medier - hur kan vänstern vara online?</title>
    <link href="https://konstellationen.org/2023/07/05/mastodonfest-rss-blogg/"/>
    <id>https://konstellationen.org/2023/07/05/mastodonfest-rss-blogg/</id>
    <published>2023-07-05T00:00:00.000Z</published>
    <updated>2023-07-06T10:20:31.007Z</updated>
    
    <content type="html"><![CDATA[<p>Välkommen på temakväll med efterhäng!</p><p>Vad finns för alternativ nu när allt fler sociala medier dör ut eller blir oanvändabara? Vad gör vänstern på internet? Lär dig om Fediversum och få hjälp att skapa konto på Mastodon. Delta i samtal om strategier för att nå ut utanför plattformarna, alternativ till sociala medier, bloggar, mail och poddar, samt käka, drick och häng med kamrater.</p><p><strong>Var:</strong> Solidaritetshuset på Södermalm, Stockholm<br><strong>När:</strong> Lördag 19 augusti kl 15:00-19:00<br>.</p>]]></content>
    
    
    <summary type="html">Välkommen på temakväll med efterhäng! Vad finns för alternativ nu när allt fler sociala medier dör ut eller blir oanvändabara? Vad gör vänstern på internet? Lär dig om Fediversum och få hjälp att skapa konto på Mastodon. Delta i samtal om strategier för att nå ut utanför plattformarna, alternativ till sociala medier, bloggar, mail och poddar, samt käka, drick och häng med kamrater.</summary>
    
    
    <content src="https://konstellationen.org/images/mastodonfest.jpg" type="image"/>
    
    
    
  </entry>

</feed>
"""

data_arbetaren_feed_sample = """<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	xmlns:dcterms="http://purl.org/dc/terms/" xmlns:media="http://search.yahoo.com/mrss/">

<channel>
	<title>Arbetaren</title>
	<atom:link href="https://www.arbetaren.se/feed/" rel="self" type="application/rss+xml" />
	<link>https://www.arbetaren.se</link>
	<description>En tidning på din sida</description>
	<lastBuildDate>Wed, 19 Jul 2023 07:49:27 +0000</lastBuildDate>
	<language>sv-SE</language>
	<sy:updatePeriod>
	hourly	</sy:updatePeriod>
	<sy:updateFrequency>
	1	</sy:updateFrequency>
	<generator>https://wordpress.org/?v=6.2.2</generator>

<image>
	<url>https://media.arbetaren.se/wp-content/uploads/2016/01/29031906/cropped-icon.png?width=64&#038;height=64</url>
	<title>Arbetaren</title>
	<link>https://www.arbetaren.se</link>
	<width>32</width>
	<height>32</height>
</image> 
	<item>
		<title>Därför måste vi prata om sexbarnsfamiljernas lyxande</title>
		<link>https://www.arbetaren.se/2023/07/19/darfor-maste-vi-prata-om-sexbarnsfamiljernas-lyxande/</link>
		
		<dc:creator><![CDATA[Edvin Alpros]]></dc:creator>
		<pubDate>Wed, 19 Jul 2023 07:49:26 +0000</pubDate>
				<category><![CDATA[Satir]]></category>
		<guid isPermaLink="false">https://www.arbetaren.se/?p=316190</guid>

					<description><![CDATA[Edvin Alpros förklarar från sin segelbåt varför sexbarnsfamiljers lyxliv på existensminimum, män med smink och VAB-missbruket är de viktigaste debatterna för varje entreprenör att driva just nu.]]></description>
										<content:encoded><![CDATA[


<p class="has-preamble-font-size">Edvin Alpros förklarar från sin segelbåt varför sexbarnsfamiljers lyxliv på existensminimum, män med smink och VAB-missbruket är de viktigaste debatterna för varje entreprenör att driva just nu.</p>



<aside class="wp-block-arbetaren-aside">
<p><strong>Edvin Alpros</strong></p>



<p>Tredje namn M-listan* i Segelby. Bit coin forskare. Ordf. Kungabilens Vänner. Inspirations föreläsare. Diplomerad kurs deltagare.<br>(*Segelbys mans grupp)</p>



<p><em>Edvin Alpros – Brev från borgerligheten</em> är Arbetarens nya satrirvinjett, av skaparen bakom Twittersatirkontot Edvin Alpros.</p>
</aside>



<p>Sommaren är en tid för reflektion och jag känner mig tillfreds med året som gått. Jag har lanserat två nya företag, Segelby Cola och Segelby Crypto. Tack vare att ledningsgruppen (jag och min fru) ofta har ledningsgruppsmöten (semester) på min segelbåt på internationellt vatten så gäller inte längre svensk arbetsrätt för mina anställda, och detta har i sin tur gjort att vi kunnat lösgöra stora utgifter ifrån personalkostnaderna&nbsp;för att i stället kunna fokusera på&nbsp;kreativ bokföring. </p>



<p>Vi hämtar inspiration från entreprenörer inom andra branscher såsom taxiföretag utan anställda, skolkoncerner utan skolor och snabbmatsleverantörer utan fordon. I Sverige kan alla lyckas om man bara lägger manken till. Själv började jag med två tomma segelbåtar och ett mindre startkapital från min far och när jag nu&nbsp;30 år senare sitter i en av segelbåtarna och läser alarmistiska rubriker&nbsp;om ”global uppvärmning” samtidigt som jag blickar ut över vattenytan så slår det mig att om havsnivån faktiskt steg så hade ju jag förmodligen varit först med&nbsp;att upptäcka det.</p>


]]></content:encoded>
					
		
		
		<dcterms:publisher>http://id.kb.se/organisations/SE5565428413</dcterms:publisher>
<dcterms:accessRights>gratis</dcterms:accessRights>
<dcterms:format>text/html</dcterms:format>
<media:group>
<media:content url='https://media.arbetaren.se/wp-content/uploads/2023/07/18093936/Edvin-Alpros.jpg' type='image/jpeg'>
<dcterms:isFormatOf>
https://media.arbetaren.se/wp-content/uploads/2023/07/18093936/Edvin-Alpros.jpg</dcterms:isFormatOf>
</media:content>
</media:group>
	</item>
</channel>
</rss>
"""

data_etc_feed_sample = """<?xml version="1.0" encoding="utf-8" ?>
<rss version="2.0" xmlns:etcFeed="https://static.etc.se/etc-rss">
  <channel>
    <title>ETC.se</title>
    <link>https://www.etc.se/</link>
    <description>Röda tidningar för ett grönare Sverige!</description>
    <language>sv</language>
    <docs>https://validator.w3.org/feed/docs/rss2.html</docs>
    <webMaster>webmaster@etc.se (ETC Webmaster)</webMaster>
              <item>
        <title>Moderat toppnamn döms till fängelse efter flera våldsbrott</title>
        <link>https://www.etc.se/inrikes/moderat-toppnamn-doems-till-faengelse-efter-flera-vaaldsbrott?utm_source=rss</link>
        <description>&lt;p&gt;När han tog plats i kommunfullmäktige var han Moderaternas mest personkryssade i den mellansvenska kommunen. Men han hade också flera brott och ett fängelsestraff bakom sig. Nu har han återigen dömts – för en rad våldsbrott mot sin familj. &lt;/p&gt;
&lt;p&gt;– Jag har inbillat mig att alla föräldrar är så här, fast jag vet att det inte är så, säger en av hans döttrar i förhör.&lt;/p&gt;</description>
        <pubDate>Mon, 24 Jul 2023 14:00:00 +0200</pubDate>
        <guid>https://www.etc.se/inrikes/moderat-toppnamn-doems-till-faengelse-efter-flera-vaaldsbrott</guid>
                <author>Carolina Lundin</author>
                            <enclosure url="https://cdn.publisher-live.etc.nu/swp/uc3g8l/media/2023072412078_1d2f11930cfcbf3b2857d6d935bccab395a701b3cbec81ccc96f0e29ac72ccb0.png" length="772096" type="image/webp" />
                <etcFeed:order>0</etcFeed:order>
        <etcFeed:genre>Nyheter</etcFeed:genre>
        <etcFeed:section>Inrikes</etcFeed:section>
        <etcFeed:urgency>3</etcFeed:urgency>
      </item>
          <item>
        <title>Mariana på Rhodos: Det kommer ta veckor att släcka bränderna</title>
        <link>https://www.etc.se/utrikes/mariana-paa-rhodos-det-kommer-ta-veckor-att-slaecka-braenderna?utm_source=rss</link>
        <description>&lt;p&gt;I en vecka har Rhodos plågats av extremhetta och eldsvåda. Värdefull natur har totalförstörts och både turister och lokalbor har tvingats fly undan skogsbränderna, samtidigt som brandmän och volontärer arbetar dygnet runt för att släcka bränderna.&lt;/p&gt;
&lt;p&gt;– Människor är helt förtvivlade, säger svenska Marina Korkida, som har bott på Rhodos i över 25 år.&lt;/p&gt;</description>
        <pubDate>Mon, 24 Jul 2023 13:20:00 +0200</pubDate>
        <guid>https://www.etc.se/utrikes/mariana-paa-rhodos-det-kommer-ta-veckor-att-slaecka-braenderna</guid>
                <author>Hanna Strid</author>
                            <enclosure url="https://cdn.publisher-live.etc.nu/swp/uc3g8l/media/20230724110720_36535de4-7720-43e6-8bd2-ffb86f16d8bf.jpg" length="66560" type="image/jpeg" />
                <etcFeed:order>1</etcFeed:order>
        <etcFeed:genre>Nyheter</etcFeed:genre>
        <etcFeed:section>Utrikes</etcFeed:section>
        <etcFeed:urgency>3</etcFeed:urgency>
      </item>
	</channel>
</rss>
"""