from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.core.paginator import Paginator
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone

from datetime import timedelta

from .models import Item
from .models import Feed
from .models import FeedCategory

def index(request):
    now = timezone.now()
    # We will only allow liked articles to be on top for 3 days
    some_time_ago = now - timedelta(days=3)
    latest_processed_items_to_show = Item.objects.filter(show_item=True, published__gte=some_time_ago).order_by("-published")[:20]
    latest_item_list = Item.objects.exclude(show_item=True, published__gte=some_time_ago).order_by("-published")[:40]
    context = {
        "latest_processed_items_to_show": latest_processed_items_to_show,
        "latest_item_list": latest_item_list,
    }
    return render(request, "index.html", context)

def about(request):
    return render(request, "about.html", {})

def contact(request):
    return render(request, "contact.html", {})

def all(request):
    item_list = Item.objects.all().order_by("-published")
    # 50 per page
    paginator = Paginator(item_list, 50)

    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)
    return render(request, "feed/index.html", {
        "feed_type": "all",
        "page_obj": page_obj,
    })

def curated(request):
    item_list = Item.objects.filter(show_item=True).order_by("-published")
    # 50 per page
    paginator = Paginator(item_list, 50)

    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)
    return render(request, "feed/index.html", {
        "feed_type": "curated",
        "page_obj": page_obj,
    })

def feed_all(request, feed_id):
    # Attempt to get the feed for the ID and 404 if we can't find it.
    feed = get_object_or_404(Feed, pk=feed_id)

    # Retrieve feed items for a specific feed, ordered by the published date with the most recent ones first.
    items = Item.objects.filter(feed_id=feed_id).order_by("-published")

    # Paginate the feed's items with 50 items per page.
    paginator = Paginator(items, 50)

    # Use the URL query parameter "page" to select which page to show.
    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)

    return render(request, "feed/index.html", {
        "feed_type": "all",
        "feed": feed,
        "page_obj": page_obj
    })

def feed_curated(request, feed_id):
    # Attempt to get the feed for the ID and 404 if we can't find it.
    feed = get_object_or_404(Feed, pk=feed_id)

    # Retrieve feed items for a specific feed, ordered by the published date with the most recent ones first.
    # Also filter the items based on "show_item" which determines if it's curated or not.
    items = Item.objects.filter(feed_id=feed_id, show_item=True).order_by("-published")

    # Paginate the feed's items with 50 items per page.
    paginator = Paginator(items, 50)

    # Use the URL query parameter "page" to select which page to show.
    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)

    return render(request, "feed/index.html", {
        "feed_type": "curated",
        "feed": feed,
        "page_obj": page_obj
    })

def detail(request, feed_item_id):
    item = get_object_or_404(Item, pk=feed_item_id)
    return render(request, "feed/item.html", {"item": item})

def list_feeds(request):
    categories = FeedCategory.objects.prefetch_related('feed_set').all()
    return render(request, "feeds/index.html", {"categories": categories})

@csrf_exempt
def api_like(request, feed_item_id):
    if request.method != "POST":
        return JsonResponse({'success': False,'message':'Method not allowed'}, status=405)

    if not request.user.is_authenticated:
        return JsonResponse({'success': False,'message':'Not logged in'}, status=401)
    
    try:
        item = Item.objects.get(pk=feed_item_id)
        print("item")
        print(item)
        item.show_item = True
        item.date_processed = timezone.now()
        item.save()

        return JsonResponse({'success': True,'message':'Like successful'})

    except Item.DoesNotExist as e:
        return JsonResponse({'success': False,'message':'Item does not exist'}, status=404)

    except Exception as e:
        print("e")
        print(e)
        return JsonResponse({'success': False,'message':'Unknown error occured when liking'}, status=500)
    

