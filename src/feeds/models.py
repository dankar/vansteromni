from django.db import models

# This is the model to describe how the rss/atom xml for a specific feed maps to Item
class ItemFeedMap(models.Model):
    item_feed_map_name = models.CharField(max_length=200)
    items = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    link = models.CharField(max_length=200)
    item_id = models.CharField(max_length=200)
    published = models.CharField(max_length=200)
    updated = models.CharField(max_length=200)
    content = models.CharField(max_length=200)
    summary = models.CharField(max_length=200)
    image_url = models.CharField(max_length=200)

    def __str__(self):
        return self.item_feed_map_name

class FeedCategory(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)  # optional description field

    def __str__(self):
        return self.name

# This is the model for the feed, e.g. for https://konstellationen.org/atom.xml
class Feed(models.Model):
    name = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    item_feed_map = models.ForeignKey(ItemFeedMap, on_delete=models.CASCADE)
    category = models.ForeignKey(FeedCategory, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.name

# This describes a generic rss/atom feed item
class Item(models.Model):
    feed = models.ForeignKey(Feed, on_delete=models.CASCADE)
    # These are things related to the moderation on this site
    date_processed = models.DateTimeField("date processed", null=True)
    show_item = models.BooleanField(default=False)
    imported = models.DateTimeField("date imported", null=True)

    # The following fields are fields taken from the rss/atom feed
    title = models.CharField(max_length=200)
    link = models.CharField(max_length=200)
    item_id = models.CharField(max_length=200)
    published = models.DateTimeField("date published", null=True, blank=True)
    updated = models.DateTimeField("date updated", null=True, blank=True)
    content = models.TextField(blank=True)
    summary = models.TextField(blank=True)
    image_url = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return "{} - {}".format(self.feed, self.title)
