#!/bin/sh

# Generates static in the static directory. This is served by nginx
echo "Collect static"
python manage.py collectstatic --no-input

echo "Migrate database"
python manage.py migrate

echo "Run gunicorn"
gunicorn mysite.wsgi:application --bind 0.0.0.0:8000