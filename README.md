# Vänsteromni

Vänsteromni is a curated list of Swedish left news articles / pod episodes / blog entries.

We will automatically import:

* https://www.etc.se/rss.xml
* https://www.arbetaren.se/feed/
* https://tidningensyre.se/feed/

Entries from those will appear in a list which a curator manually can process and "favourite" those entries that should appear in the curated feed on the site.

## Local development

This is a django site. Python is the programming language. How to set up and run using Linux:

```
virtualenv venv -p python3
source venv/bin/activate
python -m pip install -r requirements.txt
cp env.example .env
cd src
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
```

Surf to http://localhost

## Running commands

There are a couple of commands that can be run from the commandline:

```
python manage.py list_feeds
# Scrapes the feed with id 1
python manage.py scrape_feed 1
```

## Using docker compose on live

We are using docker compose to deploy live.

```
# Either "docker-compose" or "docker compose" depending on your system
docker compose build
docker compose up -d
# The first time we need to create a super user. Enter username, email, password
docker compose exec web sh -c "python manage.py createsuperuser"
```

And then use commands for scraping feeds:

```
docker compose exec web sh -c "python manage.py list_feeds"
docker compose exec web sh -c "python manage.py scrape_feed 1"
```


## Using docker compose locally

One can also use docker compose locally when developing. But then we need a different docker-compose yaml file.

```
# Either "docker-compose" or "docker compose" depending on your system
# We need to add docker-compose.dev.yml to fake the external nginx_network
docker compose -f docker-compose.yml -f docker-compose.dev.yml build
docker compose -f docker-compose.yml -f docker-compose.dev.yml up -d
```

Surf to http://localhost

## For developers

To get a hang of Django, follow [this tutorial](https://docs.djangoproject.com/en/4.2/intro/tutorial01/).

We have the following "django apps":

* entries - all posts that will be shown to the users
* feeds - all feeds for newspapers, blogs, podcasts, etc

## Testing

Just do:

```
cd src
# All tests
python manage.py test
# All tests for feeds
python manage.py test feeds
# All tests for the feeds test class FunctionsTests
python manage.py test feeds.tests.FunctionsTests
# A specific test case for feeds
python manage.py test feeds.tests.FunctionsTests.test_feed_string_to_items_arbetaren_sample
```

## Update static cascading stylesheet
To update the cascading stylesheet for the project Tailwind has to read all src-files to know what classes to package into the static file. To do this either install Tailwind [as a standalone](https://tailwindcss.com/blog/standalone-cli) or [with Node.js](https://tailwindcss.com/docs/installation). Then run:
```
npx tailwindcss -i ./src/style.css -o ./src/static/style.css --watch
```
 